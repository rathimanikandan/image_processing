import cv2
import time
source = cv2.imread("pic.png")
start_time = time.time()
final = cv2.medianBlur(source, 7)
end_time = time.time()
elapsed_time = end_time - start_time
elapsed_time_milliSeconds = elapsed_time*1000
print("code elapsed time in milliseconds is ",elapsed_time_milliSeconds)
cv2.imshow('Source_Picture', source) #Show the image
cv2.imshow('Final_Picture', final) #Show the image
cv2.waitKey(10000)