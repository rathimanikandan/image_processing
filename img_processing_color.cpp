#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<iostream>
using namespace std;
using namespace cv;
using namespace std::chrono;
#include<iostream>
#include<vector>
#include<cstdlib>
//sort the window using insertion sort
//insertion sort is best for this sorting

void insertionSort(int window[],int total)
{
    int temp, i , j;
    for(i = 0; i < total; i++){
        temp = window[i];
        for(j = i-1; j >= 0 && temp < window[j]; j--){
            window[j+1] = window[j];
        }
        window[j+1] = temp;
    }
}
void medianFilter(Mat originl,Mat filter,int dimension,int stride){
int total_elements = dimension * dimension;
//create a sliding window
      int window[total_elements];
          int slide=0;
          int middle=total_elements/2;
        filter = originl.clone();
         int imageChannels = originl.channels();
    vector<vector<int>> values (imageChannels);
        for(int y = dimension/2; y < originl.rows - 1; y+=stride){
            for(int x = dimension/2; x < originl.cols - 1; x+=stride){
                // Pick up window element
               for (int channel = 0; channel < imageChannels; channel++)
                 {
                values[channel].clear();
            	 }
                 for(int i=-(dimension/2);i<=(dimension/2);i++)
		            {	
			            for(int j=-(dimension/2);j<=(dimension/2);j++)
			                {
				                 for (int channel = 0; channel < imageChannels; channel++)
                    					{
                        					unsigned char * pixelValuePtr = filter.ptr(y + j) + ((x + i) * imageChannels) + channel;
                        					values[channel].push_back(*pixelValuePtr);
                   					 }		
			                }
		            }
		            for (int channel = 0; channel < imageChannels; channel++)
            			{
               			 sort(begin(values[channel]), end(values[channel]));
                			unsigned char * pixelValuePtr = originl.ptr(y) + (x * imageChannels) + channel;
                			*pixelValuePtr = values[channel][middle];
            }
        }
       imshow("final", originl);
       waitKey();
}
int main(int argc, char  **argv )
{
	char fname[20];
       strcpy(fname,argv[1]);
       
      Mat original, filtered;
      int dimension = atoi(argv[2]);
     
      int stride = atoi(argv[3]);
      original = imread(fname, IMREAD_COLOR);
   	if( !original.data )
      { return -1; }
      auto started = std::chrono::high_resolution_clock::now();
      medianFilter(original,filtered, dimension,stride);
      auto done = std::chrono::high_resolution_clock::now();
      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(done-started).count();
      cout << "\n";
    return 0;
}