#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<iostream>
using namespace std;
using namespace cv;
using namespace std::chrono;
//sort the window using insertion sort
//insertion sort is best for this sorting

void insertionSort(int window[],int total)
{
    int temp, i , j;
    for(i = 0; i < total; i++){
        temp = window[i];
        for(j = i-1; j >= 0 && temp < window[j]; j--){
            window[j+1] = window[j];
        }
        window[j+1] = temp;
    }
}
void medianFilter(Mat originl,Mat filter,int dimension,int stride){
 int total_elements;
      total_elements = dimension * dimension;
      int window[total_elements];
          // int *arr = new int[5];
          int slide=0;
        filter = originl.clone();
        
        for(int y = 0; y < originl.rows - 1; y+=stride){
            for(int x = 0; x < originl.cols - 1; x+=stride){
                // Pick up window element
                slide=0;
                 for(int i=-(dimension/2);i<=(dimension/2);i++)
		            {
			            for(int j=-(dimension/2);j<=(dimension/2);j++)
			                {
				                window[slide++] = originl.at<uchar>(y + i ,x + j);
			                }
		            }
                // sort the window to find median
                insertionSort(window,total_elements);
                // assign the median to centered element of the matrix
                int middle=(total_elements/2)-1;
                filter.at<uchar>(y,x) = window[middle];
            }
        }
        namedWindow("final");
        imshow("final", filter);
        namedWindow("initial");
        imshow("initial", originl);
      waitKey();
}
int main(int argc, char  **argv )
{
	char filename[100];
       strcpy(filename,argv[1]);
       int dimension = atoi(argv[2]);
     
      Mat original, filtered;
      // Load an image
      original = imread(filename, IMREAD_GRAYSCALE);
      if( !original.data )
      { return -1; }
        
      int stride = atoi(argv[3]);
      //create a sliding window
      auto started = std::chrono::high_resolution_clock::now();
      medianFilter(original,filtered, dimension,stride);
      auto done = std::chrono::high_resolution_clock::now();
      std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(done-started).count();
      cout << "\n";
      
    return 0;
}